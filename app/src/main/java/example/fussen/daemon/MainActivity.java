package example.fussen.daemon;

import android.Manifest;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.baidu.location.BDAbstractLocationListener;
import com.baidu.location.BDLocation;
import com.baidu.location.Poi;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import example.fussen.daemon.dao.LocationData;
import example.fussen.daemon.dao.LocationDataDao;
import example.fussen.daemon.service.WorkService;
import example.fussen.daemon.utils.HttpClientUtils;
import example.fussen.daemon.utils.LocationService;
import example.fussen.daemon.utils.RequestPermissionsUtil;
import example.fussen.daemon.utils.SPUtils;
import example.fussen.daemon.utils.TimeUtils;
import example.fussen.daemon.utils.ToastUtil;
import example.fussen.daemon.utils.Tools;
import example.fussen.daemon.utils.Urls;
import example.fussen.daemon.utils.updatemanager.UpdateManager;

public class MainActivity extends AppCompatActivity {

    private UpdateManager updateManager;
    private  TextView textView;
    private  TextView lastTime;
    private String uuid;
    static MyHandler myHandler;
    private LocationService locService;
    private MsgReceiver msgReceiver;
    public static String RECEIVER="example.fussen.daemon.MainActivity";
    private LocationDataDao locationDataDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        if (RequestPermissionsUtil.requestPer(MainActivity.this, Manifest.permission.RECEIVE_BOOT_COMPLETED, 1)) {
//            promptNoPermission(R.string.btn_setting);
//        } else {
//            promptNoPermission(R.string.btn_setting);
//        }
        initView();
    }

    private void initView(){
        myHandler = new MyHandler(MainActivity.this);
        textView= (TextView) findViewById(R.id.text);
        lastTime= (TextView) findViewById(R.id.lastTime);
        uuid=Tools.getUdid(MainActivity.this);
        textView.setText("唯一设备码："+uuid);
        updateManager=new UpdateManager();
        updateManager.checkAppUpdate(MainActivity.this, true, Urls.UPDATE_INFO);

        msgReceiver = new MsgReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(RECEIVER);
        registerReceiver(msgReceiver, intentFilter);
        boolean isfrist= (boolean) SPUtils.get(MainActivity.this,SPUtils.ISFRIST,false);

        locationDataDao=App.getInstance().getDaoSession().getLocationDataDao();

        List<LocationData> locationDataList=locationDataDao.loadAll();
        ArrayList arrayList=new ArrayList();
        for (LocationData locationData:locationDataList){
            arrayList.add(locationData.getData());


        }
        if (arrayList.size()>0){
            new Thread(new OutputRunnable(arrayList.toString())).start();
        }

    }
    private void initMap(){
        // -----------location config ------------
        locService = ((App) getApplication()).locationService;
        //获取locationservice实例，建议应用中只初始化1个location实例，然后使用，可以参考其他示例的activity，都是通过此种方式获取locationservice实例的
        locService.registerListener(mListener);
        //注册监听
        locService.setLocationOption(locService.getDefaultLocationClientOption());
        locService.start();
        locService.getClient().enableLocInForeground(1,new Notification());

    }

    @Override
    protected void onResume() {
        super.onResume();
        String time= (String) SPUtils.get(App.getInstance(),SPUtils.LASTTIME,"---");
        Log.d("--------------",time);
        lastTime.setText("最后一次发送时间："+time+"");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (updateManager != null) {
            updateManager.dismissPopupWindow();
        }

    }
    //权限请求回调
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                RequestPermissionsUtil.onRequestPermissionsResult(MainActivity.this, Manifest.permission.RECEIVE_BOOT_COMPLETED, requestCode, permissions, grantResults);
                break;
            default:
                break;
        }

    }

    /**
     * 请求权限是底部弹出提示
     */

    private void promptNoPermission(int res) {
        Snackbar.make(MainActivity.this.findViewById(android.R.id.content), res, Snackbar.LENGTH_LONG).setAction(R.string.btn_setting, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
//                        Uri.parse("package:" + MainActivity.this.getPackageName()));
//                startActivity(intent);
                Intent intent = new Intent(MainActivity.this.getPackageName());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ComponentName comp = new ComponentName("com.huawei.systemmanager", "com.huawei.permissionmanager.ui.MainActivity");
                intent.setComponent(comp);
                startActivity(intent);
            }
        }).show();
    }
    /**
     * 防止华为机型未加入白名单时按返回键回到桌面再锁屏后几秒钟进程被杀
     */
    @Override
    public void onBackPressed() {
        Intent launcherIntent = new Intent(Intent.ACTION_MAIN);
        launcherIntent.addCategory(Intent.CATEGORY_HOME);
        startActivity(launcherIntent);
    }

    /*****
     *
     * 定位结果回调，重写onReceiveLocation方法，可以直接拷贝如下代码到自己工程中修改
     *
     */
    private BDAbstractLocationListener mListener = new BDAbstractLocationListener() {

        @Override
        public void onReceiveLocation(BDLocation location) {
            // TODO Auto-generated method stub
            String ltion=null;
            if (null != location && location.getLocType() != BDLocation.TypeServerError) {
                StringBuffer sb = new StringBuffer(256);
                sb.append("time : ");
                /**
                 * 时间也可以使用systemClock.elapsedRealtime()方法 获取的是自从开机以来，每次回调的时间；
                 * location.getTime() 是指服务端出本次结果的时间，如果位置不发生变化，则时间不变
                 */
                sb.append(location.getTime());
                sb.append("\nlocType : ");// 定位类型
                sb.append(location.getLocType());
                sb.append("\nlocType description : ");// *****对应的定位类型说明*****
                sb.append(location.getLocTypeDescription());
                sb.append("\nlatitude : ");// 纬度
                sb.append(location.getLatitude());
                sb.append("\nlontitude : ");// 经度
                sb.append(location.getLongitude());
                sb.append("\nradius : ");// 半径
                sb.append(location.getRadius());
                sb.append("\nCountryCode : ");// 国家码
                sb.append(location.getCountryCode());
                sb.append("\nCountry : ");// 国家名称
                sb.append(location.getCountry());
                sb.append("\ncitycode : ");// 城市编码
                sb.append(location.getCityCode());
                sb.append("\ncity : ");// 城市
                sb.append(location.getCity());
                sb.append("\nDistrict : ");// 区
                sb.append(location.getDistrict());
                sb.append("\nStreet : ");// 街道
                sb.append(location.getStreet());
                sb.append("\naddr : ");// 地址信息
                sb.append(location.getAddrStr());
                sb.append("\nUserIndoorState: ");// *****返回用户室内外判断结果*****
                sb.append(location.getUserIndoorState());
                sb.append("\nDirection(not all devices have value): ");
                sb.append(location.getDirection());// 方向
                sb.append("\nlocationdescribe: ");
                sb.append(location.getLocationDescribe());// 位置语义化信息
                sb.append("\nPoi: ");// POI信息
                if (location.getPoiList() != null && !location.getPoiList().isEmpty()) {
                    for (int i = 0; i < location.getPoiList().size(); i++) {
                        Poi poi = (Poi) location.getPoiList().get(i);
                        sb.append(poi.getName() + ";");
                    }
                }
                if (location.getLocType() == BDLocation.TypeGpsLocation) {// GPS定位结果
                    sb.append("\nspeed : ");
                    sb.append(location.getSpeed());// 速度 单位：km/h
                    sb.append("\nsatellite : ");
                    sb.append(location.getSatelliteNumber());// 卫星数目
                    sb.append("\nheight : ");
                    sb.append(location.getAltitude());// 海拔高度 单位：米
                    sb.append("\ngps status : ");
                    sb.append(location.getGpsAccuracyStatus());// *****gps质量判断*****
                    sb.append("\ndescribe : ");
                    sb.append("gps定位成功");
                } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {// 网络定位结果
                    // 运营商信息
                    if (location.hasAltitude()) {// *****如果有海拔高度*****
                        sb.append("\nheight : ");
                        sb.append(location.getAltitude());// 单位：米
                    }
                    sb.append("\noperationers : ");// 运营商信息
                    sb.append(location.getOperators());
                    sb.append("\ndescribe : ");
                    sb.append("网络定位成功");
                } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
                    sb.append("\ndescribe : ");
                    sb.append("离线定位成功，离线定位结果也是有效的");
                } else if (location.getLocType() == BDLocation.TypeServerError) {
                    sb.append("\ndescribe : ");
                    sb.append("服务端网络定位失败，可以反馈IMEI号和大体定位时间到loc-bugs@baidu.com，会有人追查原因");
                } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
                    sb.append("\ndescribe : ");
                    sb.append("网络不同导致定位失败，请检查网络是否通畅");
                } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
                    sb.append("\ndescribe : ");
                    sb.append("无法获取有效定位依据导致定位失败，一般是由于手机的原因，处于飞行模式下一般会造成这种结果，可以试着重启手机");
                }
                //解析定位结果，
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("lo", location.getLongitude());
                    jsonObject.put("la", location.getLatitude());
                    ltion=jsonObject.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("----------",sb.toString());
            }else {
//                try {
////                    ltion=getLngAndLat(WorkService.this);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
            }
            String uuid = Tools.getUdid(MainActivity.this);
            Map<String, String> map = new HashMap<String, String>();
            map.put("location", ltion);
            map.put("uuid", uuid);
            JSONObject jsonmap=new JSONObject(map);
            Log.d("-----initLocation",jsonmap.toString());
            locService.stop();
            Log.d("-----initLocation",locService.isStart()+"");
//            new Thread(new OutputRunnable(jsonmap.toString())).start();

        }

    };
    class OutputRunnable implements Runnable {

        String locationData;
        public OutputRunnable(String json){
            this.locationData=json;
        }
        @Override
        public void run() {
            Log.d("-------------", "------------");
            output(locationData);

        }
    }

    public void output(String locationData) {


        try {

            String url = "http://gps.sxzhibo.com:8888/gps";
            Log.d("-------------json", locationData);
            String result = HttpClientUtils.doJsonPost(url,locationData);
            Log.d("-------------", result);
            if (result.contains("成功")){
                if (locationDataDao!=null){
                    locationDataDao.deleteAll();
                }
            }
            myHandler.sendEmptyMessage(1);
        } catch (Exception e) {
        } finally {
        }
    }

    private class MyHandler extends Handler {


        WeakReference<Context> mActivity;

        MyHandler(Context context) {
            mActivity = new WeakReference<Context>(context);
        }

        @Override
        public void handleMessage(Message msg) {
            Context activity = mActivity.get();
            if (activity == null) {
                return;
            }
            switch (msg.what) {
                case 1: //
                    String time=TimeUtils.getCurrentTime();
                    SPUtils.put(MainActivity.this,SPUtils.LASTTIME, time);
                    lastTime.setText("最后发送时间："+time);
                    break;
                default:
                    break;
            }
        }
    }
    @Override
    protected void onDestroy() {
        //注销广播
        unregisterReceiver(msgReceiver);
        super.onDestroy();
    }
    /**
     * 广播接收器
     * @author len
     *
     */
    public class MsgReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            //拿到进度，更新UI
           String time=intent.getStringExtra("time");
            lastTime.setText("发送时间："+ time+"");
        }

    }
}
