package example.fussen.daemon.service;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;

import com.baidu.location.BDAbstractLocationListener;
import com.baidu.location.BDLocation;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.Poi;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import example.fussen.daemon.App;
import example.fussen.daemon.MainActivity;
import example.fussen.daemon.R;
import example.fussen.daemon.dao.LocationData;
import example.fussen.daemon.dao.LocationDataDao;
import example.fussen.daemon.receiver.AlarmReceiver;
import example.fussen.daemon.receiver.MyWakefulReceiver;
import example.fussen.daemon.receiver.WakeUpReceiver;
import example.fussen.daemon.utils.FileUtil;
import example.fussen.daemon.utils.HttpClientUtils;
import example.fussen.daemon.utils.LocationService;
import example.fussen.daemon.utils.NotificationUtils;
import example.fussen.daemon.utils.SPUtils;
import example.fussen.daemon.utils.TimeUitl;
import example.fussen.daemon.utils.TimeUtils;
import example.fussen.daemon.utils.ToastUtil;
import example.fussen.daemon.utils.Tools;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;

/**
 * Created by Fussen on 2017/2/21.
 * <p>
 * 需要正常工作的服务
 */

public class WorkService extends Service {

    static final int HASH_CODE = 1;

    //是否 任务完成, 不再需要服务运行?
    public static boolean sShouldStopService;

    public static Subscription sSubscription;

    static String TAG = "[WorkService]";

    public static Intent serviceIntent;
    static MyHandler myHandler;
    private LocationService locService;
    private NotificationUtils mNotificationUtils;
    private Notification notification;
    private Intent intent = new Intent(MainActivity.RECEIVER);
    StringBuffer sb1 = new StringBuffer(256);
    StringBuffer sb2 = new StringBuffer(256);

    private LocationDataDao locationDataDao;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.serviceIntent = intent;
        myHandler = new MyHandler(WorkService.this);
        locationDataDao=App.getInstance().getDaoSession().getLocationDataDao();
        return onStart(intent, flags, startId);
    }

    private void initTimerTask() {
// 初始化定时
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                myHandler.sendEmptyMessage(1);
//            }
//        }).start();
//        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
//        int five = 5000; // 这是5s
//        long triggerAtTime = SystemClock.elapsedRealtime() + five;
//        Intent i = new Intent(this, AlarmReceiver.class);
//        PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);
//        manager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, triggerAtTime, pi);
    }

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {

        @Override
        public void run() {
            // handler自带方法实现定时器
            try {
                handler.postDelayed(this, 1000*60*2);
                System.out.println("-------------exception...");
                myHandler.sendEmptyMessage(1);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("exception..." + e.getMessage());
            }
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        onStart(intent, 0, 0);
        return null;
    }


    /**
     * 最近任务列表中划掉卡片时回调
     */
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        onEnd(rootIntent);
    }

    /**
     * 设置-正在运行中停止服务时回调
     */
    @Override
    public void onDestroy() {
        onEnd(null);

        String text = "time: " + TimeUitl.parse(System.currentTimeMillis()) + "          =============WorkService死了=============";
        FileUtil.writeFile(text);
    }


    /**
     * 系统会根据不同的内存状态来回调
     *
     * @param level
     */
    @Override
    public void onTrimMemory(int level) {

        String text = "time: " + TimeUitl.parse(System.currentTimeMillis()) + "          =============onTrimMemory=============";
        FileUtil.writeFile(text);

        switch (level) {
            case TRIM_MEMORY_RUNNING_MODERATE:   //表示应用程序正常运行，并且不会被杀掉。但是目前手机的内存已经有点低了，系统可能会开始根据LRU缓存规则来去杀死进程了

                String TRIM_MEMORY_RUNNING_MODERATE = "time: " + TimeUitl.parse(System.currentTimeMillis()) + "          =============目前手机的内存已经有点低了，系统可能会开始根据LRU缓存规则来去杀死进程了=============";
                FileUtil.writeFile(TRIM_MEMORY_RUNNING_MODERATE);
                break;
            case TRIM_MEMORY_RUNNING_LOW:        //目前手机的内存已经非常低了

                String TRIM_MEMORY_RUNNING_LOW = "time: " + TimeUitl.parse(System.currentTimeMillis()) + "          =============目前手机的内存已经非常低了=============";
                FileUtil.writeFile(TRIM_MEMORY_RUNNING_LOW);
                break;
            case TRIM_MEMORY_RUNNING_CRITICAL:  //系统已经根据LRU缓存规则杀掉了大部分缓存的进程了

                String TRIM_MEMORY_RUNNING_CRITICAL = "time: " + TimeUitl.parse(System.currentTimeMillis()) + "          =============系统已经根据LRU缓存规则杀掉了大部分缓存的进程了=============";
                FileUtil.writeFile(TRIM_MEMORY_RUNNING_CRITICAL);
                break;
            default:
                String defaultLevel = "time: " + TimeUitl.parse(System.currentTimeMillis()) + "          =============defaultLevel=============" + level;
                FileUtil.writeFile(defaultLevel);
                break;
        }
    }

    void startService() {
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 1000);
//        initTimerTask();
        //检查服务是否不需要运行
        if (sShouldStopService) return;
        //若还没有取消订阅，说明任务仍在运行，为防止重复启动，直接 return
        if (sSubscription != null && !sSubscription.isUnsubscribed()) return;

        //----------业务逻辑----------

        System.out.println("检查磁盘中是否有上次销毁时保存的数据");


        //================本地记录日志================
        String content = "time: " + TimeUitl.parse(System.currentTimeMillis()) + "          检查磁盘中是否有上次销毁时保存的数据";

        FileUtil.writeFile(content);
        //=============本地记录日志==============

        sSubscription = Observable
                .interval(6, TimeUnit.MINUTES)
                //取消任务时取消定时唤醒
                .doOnUnsubscribe(new Action0() {
                    @Override
                    public void call() {
                        System.out.println("保存数据到磁盘。");
                        App.sApp.sendBroadcast(new Intent(WakeUpReceiver.ACTION_CANCEL_JOB_ALARM_SUB));

                        //=============本地记录日志=============
                        String content = "time: " + TimeUitl.parse(System.currentTimeMillis()) + "          保存数据到磁盘";
                        FileUtil.writeFile(content);
                        //=============本地记录日志==============
                    }
                }).subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long count) {

                        System.out.println("每 30 秒采集一次数据... count = " + count);
//                        myHandler.sendEmptyMessage(1);
                        //=============本地记录日志=============
                        String content = "time: " + TimeUitl.parse(System.currentTimeMillis()) + "          每 30 秒采集一次数据... count =" + count;
                        FileUtil.writeFile(content);
                        //=============本地记录日志==============


                        if (count > 0 && count % 18 == 0) {

                            System.out.println("保存数据到磁盘。 saveCount = " + (count / 18 - 1));

                            String text = "time: " + TimeUitl.parse(System.currentTimeMillis()) + "          ================保存数据到磁盘。 saveCount=" + (count / 18 - 1);
                            FileUtil.writeFile(text);
                        }
                    }
                });

        //----------业务逻辑----------
    }


    /**
     * 停止服务并取消定时唤醒
     * <p>
     * 停止服务使用取消订阅的方式实现，而不是调用 Context.stopService(Intent name)。因为：
     * 1.stopService 会调用 Service.onDestroy()，而 WorkService 做了保活处理，会把 Service 再拉起来；
     * 2.我们希望 WorkService 起到一个类似于控制台的角色，即 WorkService 始终运行 (无论任务是否需要运行)，
     * 而是通过 onStart() 里自定义的条件，来决定服务是否应当启动或停止。
     */
    public static void stopService() {
        //我们现在不再需要服务运行了, 将标志位置为 true
        sShouldStopService = true;
        //取消对任务的订阅
        if (sSubscription != null) sSubscription.unsubscribe();
        //取消 Job / Alarm / Subscription
        App.sApp.sendBroadcast(new Intent(WakeUpReceiver.ACTION_CANCEL_JOB_ALARM_SUB));

        //停止唤醒CPU
        MyWakefulReceiver.completeWakefulIntent(serviceIntent);
    }


    /**
     * 1.防止重复启动，可以任意调用startService(Intent i);
     * 2.利用漏洞启动前台服务而不显示通知;
     * 3.在子线程中运行定时任务，处理了运行前检查和销毁时保存的问题;
     * 4.启动守护服务;
     * 5.守护 Service 组件的启用状态, 使其不被 MAT 等工具禁用.
     */
    int onStart(Intent intent, int flags, int startId) {
        //启动前台服务而不显示通知的漏洞已在 API Level 25 修复，大快人心！
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
            //利用漏洞在 API Level 17 及以下的 Android 系统中，启动前台服务而不显示通知
            startForeground(HASH_CODE, new Notification());
            //利用漏洞在 API Level 18 及以上的 Android 系统中，启动前台服务而不显示通知
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                startService(new Intent(App.sApp, WorkNotificationService.class));
        }

        //启动守护服务，运行在:watch子进程中
        startService(new Intent(App.sApp, DaemonService.class));

        //----------业务逻辑----------

        //实际使用时，根据需求，将这里更改为自定义的条件，判定服务应当启动还是停止 (任务是否需要运行)
        if (sShouldStopService) stopService();
        else startService();

        //----------业务逻辑----------

        getPackageManager().setComponentEnabledSetting(new ComponentName(getPackageName(), DaemonService.class.getName()),
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

        return START_STICKY;
    }


    void onEnd(Intent rootIntent) {
        System.out.println("保存数据到磁盘。");
        startService(new Intent(App.sApp, WorkService.class));
        startService(new Intent(App.sApp, DaemonService.class));
    }

    public static class WorkNotificationService extends Service {

        /**
         * 利用漏洞在 API Level 18 及以上的 Android 系统中，启动前台服务而不显示通知
         */
        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            startForeground(WorkService.HASH_CODE, new Notification());
            stopSelf();
            return START_STICKY;
        }

        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }
    }

    private class MyHandler extends Handler {


        WeakReference<Context> mActivity;

        MyHandler(Context context) {
            mActivity = new WeakReference<Context>(context);
        }

        @Override
        public void handleMessage(Message msg) {
            Context activity = mActivity.get();
            if (activity == null) {
                return;
            }
            switch (msg.what) {
                case 1: //
                    Log.d("-------------", "locService.start();");
                    String currentTime = TimeUtils.getCurrentTime();
//                    sb1.append("\n" + s);//
//                    SPUtils.put(App.getInstance(), SPUtils.aaaaaaaaa, sb1);
                    String lasttime= (String) SPUtils.get(App.getInstance(), SPUtils.LASTTIME, "--");
                    Long minutes=TimeUtils.subtract(currentTime,lasttime);
                    if (minutes>4){
                        initMap();
                    }

                    break;
                case 2: //
                    String time = (String) msg.obj;
                    Log.d("-------------time", time);
//                    sb2.append("\n" + time);//
//                    SPUtils.put(App.getInstance(), SPUtils.bbbbbbbbbbbbb, sb2);
                    SPUtils.put(App.getInstance(), SPUtils.LASTTIME, time);
                    intent.putExtra("time", time);
                    Log.d("-------------time", SPUtils.get(App.getInstance(), SPUtils.LASTTIME, "--") + "");
                    sendBroadcast(intent);
                    break;
                default:
                    break;
            }
        }
    }

    class OutputRunnable implements Runnable {

        String json;
        String locaDate;

        public OutputRunnable(String json,String locaDate) {
            this.json = json;
            this.locaDate = locaDate;
        }

        @Override
        public void run() {
            Log.d("-------------", "------------");
            output(json,locaDate);
        }
    }

    public void output(String json,String locaDate) {

        String time = null;
        try {
//            String LaAndLo = getLngAndLat(WorkService.this);
//            String LaAndLo = "";
//            String uuid = Tools.getUdid(WorkService.this);
//            Map<String, String> map = new HashMap<String, String>();
//            map.put("location", LaAndLo);
//            map.put("uuid", uuid);
//            Log.d("-------------", map.toString());


            String url = "http://gps.sxzhibo.com:8888/gps";
//            String param="LaAndLo="+LaAndLo+"&uuid="+uuid;
//            JSONObject jsonObject=new JSONObject(map);
            Log.d("-------------json", json);
            JSONObject jsonObject=new JSONObject(json);
            ArrayList arrayList=new ArrayList();
            arrayList.add(jsonObject);
            String result = HttpClientUtils.doJsonPost(url,arrayList.toString());
            Log.d("-------------", result);
            if (!result.contains("成功")){
                if (locationDataDao!=null){
                    locationDataDao.insert(new LocationData(null,json,false));
                }
            }
            time = locaDate;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            time = locaDate + "\n" + e.getMessage();
        } finally {
            Message message = new Message();
            message.what = 2;
            message.obj = time;
            myHandler.sendMessage(message);
        }
    }

    private void initMap() {
        // -----------location config ------------
        locService = ((App) getApplication()).locationService;
//        //获取locationservice实例，建议应用中只初始化1个location实例，然后使用，可以参考其他示例的activity，都是通过此种方式获取locationservice实例的
        locService.registerListener(mListener);
//        //注册监听
        locService.setLocationOption(locService.getDefaultLocationClientOption());
        locService.start();

//
////        //设置后台定位
////        //android8.0及以上使用NotificationUtils
        if (Build.VERSION.SDK_INT >= 26) {
            mNotificationUtils = new NotificationUtils(this);
            Notification.Builder builder2 = mNotificationUtils.getAndroidChannelNotification
                    ("适配android 8限制后台定位功能", "正在后台定位");
            notification = builder2.build();
        } else {
            //获取一个Notification构造器
            Notification.Builder builder = new Notification.Builder(WorkService.this);
            Intent nfIntent = new Intent(WorkService.this, MainActivity.class);

            builder.setContentIntent(PendingIntent.
                    getActivity(App.getInstance(), 0, nfIntent, 0)) // 设置PendingIntent
                    .setContentTitle("后台定位功能") // 设置下拉列表里的标题
                    .setSmallIcon(R.mipmap.logo_icon) // 设置状态栏内的小图标
                    .setContentText("正在后台定位") // 设置上下文内容
                    .setWhen(System.currentTimeMillis()); // 设置该通知发生的时间

            notification = builder.build(); // 获取构建好的Notification
        }
//        notification.defaults = Notification.DEFAULT_SOUND; //设置为默认的声音
//        locService.getClient().enableLocInForeground(1, new Notification());
        locService.getClient().enableLocInForeground(1, notification);

    }

    /*****
     *
     * 定位结果回调，重写onReceiveLocation方法，可以直接拷贝如下代码到自己工程中修改
     *
     */
    private BDAbstractLocationListener mListener = new BDAbstractLocationListener() {

        @Override
        public void onReceiveLocation(BDLocation location) {
            // TODO Auto-generated method stub
//            String ltion = null;
            if (null != location && location.getLocType() != BDLocation.TypeServerError) {
                StringBuffer sb = new StringBuffer(256);
                sb.append("time : ");
                /**
                 * 时间也可以使用systemClock.elapsedRealtime()方法 获取的是自从开机以来，每次回调的时间；
                 * location.getTime() 是指服务端出本次结果的时间，如果位置不发生变化，则时间不变
                 */
                sb.append(location.getTime());
                sb.append("\nlocType : ");// 定位类型
                sb.append(location.getLocType());
                sb.append("\nlocType description : ");// *****对应的定位类型说明*****
                sb.append(location.getLocTypeDescription());
                sb.append("\nlatitude : ");// 纬度
                sb.append(location.getLatitude());
                sb.append("\nlontitude : ");// 经度
                sb.append(location.getLongitude());
                sb.append("\nradius : ");// 半径
                sb.append(location.getRadius());
                sb.append("\nCountryCode : ");// 国家码
                sb.append(location.getCountryCode());
                sb.append("\nCountry : ");// 国家名称
                sb.append(location.getCountry());
                sb.append("\ncitycode : ");// 城市编码
                sb.append(location.getCityCode());
                sb.append("\ncity : ");// 城市
                sb.append(location.getCity());
                sb.append("\nDistrict : ");// 区
                sb.append(location.getDistrict());
                sb.append("\nStreet : ");// 街道
                sb.append(location.getStreet());
                sb.append("\naddr : ");// 地址信息
                sb.append(location.getAddrStr());
                sb.append("\nUserIndoorState: ");// *****返回用户室内外判断结果*****
                sb.append(location.getUserIndoorState());
                sb.append("\nDirection(not all devices have value): ");
                sb.append(location.getDirection());// 方向
                sb.append("\nlocationdescribe: ");
                sb.append(location.getLocationDescribe());// 位置语义化信息
                sb.append("\nPoi: ");// POI信息
                if (location.getPoiList() != null && !location.getPoiList().isEmpty()) {
                    for (int i = 0; i < location.getPoiList().size(); i++) {
                        Poi poi = (Poi) location.getPoiList().get(i);
                        sb.append(poi.getName() + ";");
                    }
                }
                if (location.getLocType() == BDLocation.TypeGpsLocation) {// GPS定位结果
                    sb.append("\nspeed : ");
                    sb.append(location.getSpeed());// 速度 单位：km/h
                    sb.append("\nsatellite : ");
                    sb.append(location.getSatelliteNumber());// 卫星数目
                    sb.append("\nheight : ");
                    sb.append(location.getAltitude());// 海拔高度 单位：米
                    sb.append("\ngps status : ");
                    sb.append(location.getGpsAccuracyStatus());// *****gps质量判断*****
                    sb.append("\ndescribe : ");
                    sb.append("gps定位成功");
                } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {// 网络定位结果
                    // 运营商信息
                    if (location.hasAltitude()) {// *****如果有海拔高度*****
                        sb.append("\nheight : ");
                        sb.append(location.getAltitude());// 单位：米
                    }
                    sb.append("\noperationers : ");// 运营商信息
                    sb.append(location.getOperators());
                    sb.append("\ndescribe : ");
                    sb.append("网络定位成功");
                } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
                    sb.append("\ndescribe : ");
                    sb.append("离线定位成功，离线定位结果也是有效的");
                } else if (location.getLocType() == BDLocation.TypeServerError) {
                    sb.append("\ndescribe : ");
                    sb.append("服务端网络定位失败，可以反馈IMEI号和大体定位时间到loc-bugs@baidu.com，会有人追查原因");
                } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
                    sb.append("\ndescribe : ");
                    sb.append("网络不同导致定位失败，请检查网络是否通畅");
                } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
                    sb.append("\ndescribe : ");
                    sb.append("无法获取有效定位依据导致定位失败，一般是由于手机的原因，处于飞行模式下一般会造成这种结果，可以试着重启手机");
                }
//                //解析定位结果，
//                JSONObject jsonObject = new JSONObject();
//                try {
//                    jsonObject.put("lo", location.getLongitude());
//                    jsonObject.put("la", location.getLatitude());
//                    ltion = jsonObject.toString();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
                Log.d("----------", sb.toString());
                String uuid = Tools.getUdid(WorkService.this);
                Map<String, String> map = new HashMap<String, String>();
                map.put("createDate", location.getTime());
                map.put("long", location.getLongitude()+"");
                map.put("lati", location.getLatitude()+"");
                map.put("uuid", uuid);
                JSONObject jsonmap = new JSONObject(map);
                Log.d("-----initLocation", jsonmap.toString());
                locService.stop();
                Log.d("-----initLocation", locService.isStart() + "");
                new Thread(new OutputRunnable(jsonmap.toString(),location.getTime())).start();
            }
//            else {
//                try {
//                    ltion = getLngAndLat(WorkService.this);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }


        }

    };

    /**
     * 获取经纬度
     *
     * @param context
     * @return
     */
    private String getLngAndLat(Context context) throws JSONException {
        double latitude = 0.0;
        double longitude = 0.0;
        String type = null;
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {  //从gps获取经纬度
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                type = "gps";
            } else {//当GPS信号弱没获取到位置的时候又从网络获取
                return getLngAndLatWithNetwork();
            }
        } else {    //从网络获取经纬度
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, locationListenerss);
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                type = "wifi";
            }
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("lo", longitude);
        jsonObject.put("la", latitude);
        jsonObject.put("type", type);
        return jsonObject.toString();
    }

    //从网络获取经纬度
    public String getLngAndLatWithNetwork() throws JSONException {
        double latitude = 0.0;
        double longitude = 0.0;
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, locationListenerss);
        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("lo", longitude);
        jsonObject.put("la", latitude);
        jsonObject.put("type", "wifi");
        return jsonObject.toString();
    }


    LocationListener locationListenerss = new LocationListener() {

        // Provider的状态在可用、暂时不可用和无服务三个状态直接切换时触发此函数
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        // Provider被enable时触发此函数，比如GPS被打开
        @Override
        public void onProviderEnabled(String provider) {

        }

        // Provider被disable时触发此函数，比如GPS被关闭
        @Override
        public void onProviderDisabled(String provider) {

        }

        //当坐标改变时触发此函数，如果Provider传进相同的坐标，它就不会被触发
        @Override
        public void onLocationChanged(Location location) {
        }
    };

}
