package example.fussen.daemon.dao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/**
 * 项目名称：DaemonProject1
 * 类描述：
 * 创建人：liubing
 * 创建时间：2018-12-21 13:09
 * 修改人：Administrator
 * 修改时间：2018-12-21 13:09
 * 修改备注：
 */
@Entity
public class LocationData {

    @Id
    private Long id;

    @Property(nameInDb = "DATA")
    private String data;

    @Property(nameInDb = "ISSEND")
    private boolean isSend;

    @Generated(hash = 289582090)
    public LocationData(Long id, String data, boolean isSend) {
        this.id = id;
        this.data = data;
        this.isSend = isSend;
    }

    @Generated(hash = 1606831457)
    public LocationData() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean getIsSend() {
        return this.isSend;
    }

    public void setIsSend(boolean isSend) {
        this.isSend = isSend;
    }
}
