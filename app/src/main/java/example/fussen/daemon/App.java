package example.fussen.daemon;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Application;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;

import com.baidu.mapapi.SDKInitializer;

import example.fussen.daemon.dao.DaoMaster;
import example.fussen.daemon.dao.DaoSession;
import example.fussen.daemon.receiver.ScreenReceiver;
import example.fussen.daemon.service.WorkService;
import example.fussen.daemon.utils.LocationService;


/**
 * Created by Fussen on 2017/2/21.
 */

public class App extends Application {


    static String ACCOUNT_TYPE = "example.fussen.daemon";
    static String CONTENT_AUTHORITY = "example.fussen.daemon.provider";
    public static App sApp;
    public LocationService locationService;


    private DaoMaster.DevOpenHelper mHelper;
    private SQLiteDatabase db;
    private DaoMaster mDaoMaster;
    private DaoSession mDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        sApp = this;


        //我们现在需要服务运行, 将标志位重置为 false
        WorkService.sShouldStopService = false;
        startService(new Intent(this, WorkService.class));

        registerScreenReceiver();

        addAccount();


        locationService = new LocationService(getApplicationContext());
        SDKInitializer.initialize(this);
        setDatabase();
    }

    /**
     * 单例模式中获取唯一的ExitApplication 实例
     */

    public static App getInstance() {
        if (null == sApp) {
            sApp = new App();
        }
        return sApp;
    }


    /**
     * 启用账户自动同步
     */
    public void addAccount() {
        //添加账号

        AccountManager accountManager = (AccountManager) getSystemService(ACCOUNT_SERVICE);

        Account account = null;

        Account[] accountsByType = accountManager.getAccountsByType(ACCOUNT_TYPE);

        if (accountsByType.length > 0) {
            account = accountsByType[0];
        } else {
            account = new Account(getString(R.string.app_name), ACCOUNT_TYPE);
        }

        if (accountManager.addAccountExplicitly(account, null, null)) {

            //开启同步，设置同步周期  30秒
            ContentResolver.setIsSyncable(account, CONTENT_AUTHORITY, 1);
            ContentResolver.setSyncAutomatically(account, CONTENT_AUTHORITY, true);
            ContentResolver.addPeriodicSync(account, CONTENT_AUTHORITY, Bundle.EMPTY, 5);
        }

    }


    /**
     * 注册screen状态广播接收器
     */
    private void registerScreenReceiver() {

        ScreenReceiver screenReceiver = new ScreenReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        filter.addAction((ConnectivityManager.CONNECTIVITY_ACTION));
        registerReceiver(screenReceiver, filter);
    }

    public static Context getContext() {
        return sApp;
    }

    /**
     * 设置数据库
     */

    private void setDatabase(){
        //    // 通过 DaoMaster 的内部类 DevOpenHelper，你可以得到一个便利的 SQLiteOpenHelper 对象。
//   // 可能你已经注意到了，你并不需要去编写「CREATE TABLE」这样的 SQL 语句，因为 greenDAO 已经帮你做了。
//        // 注意：默认的 DaoMaster.DevOpenHelper 会在数据库升级时，删除所有的表，意味着这将导致数据的丢失。
//        // 所以，在正式的项目中，你还应该做一层封装，来实现数据库的安全升级。
//        // 此处sport-db表示数据库名称 可以任意填写
        mHelper=new DaoMaster.DevOpenHelper(this,"location.db",null);
        db = mHelper.getWritableDatabase();
        mDaoMaster = new DaoMaster(db);
        mDaoSession = mDaoMaster.newSession();
    }

    public DaoSession getDaoSession(){
        return mDaoSession;
    }

    public SQLiteDatabase getDb(){
        return db;
    }



}
