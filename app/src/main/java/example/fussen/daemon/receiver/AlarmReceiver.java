package example.fussen.daemon.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import example.fussen.daemon.App;
import example.fussen.daemon.service.WorkService;
import example.fussen.daemon.utils.SPUtils;
import example.fussen.daemon.utils.TimeUtils;

/**
 * 项目名称：DaemonProject1
 * 类描述：
 * 创建人：liubing
 * 创建时间：2018-12-19 11:05
 * 修改人：Administrator
 * 修改时间：2018-12-19 11:05
 * 修改备注：
 */
public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, WorkService.class);
        context.startService(i);
    }
}
