package example.fussen.daemon.utils;

import android.text.TextUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Administrator on 2016/5/31.
 */
public class TimeUtils {

    public static String Time2String(Date date, String format) {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    public static String Time2String(Date date) {
        return Time2String(date, "yyyy-MM-dd HH:mm:ss:SSS");
    }

    public static Date String2Time(String date, String format) {
        DateFormat df = new SimpleDateFormat(format);
        try {
            return df.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public static Date String2Time(String date) {
        return String2Time(date, "yyyy-MM-dd HH:mm:ss:SSS");
    }

    public static String getWeek(int index) {
        String[] week = {"周日", "周一", "周二", "周三", "周四", "周五", "周六"};
        return week[index % week.length];
    }

    public static long getDiffDays(Date date1, Date date2) {
        long time1 = date1.getTime();
        long time2 = date2.getTime();
        long result = Math.abs(time2 - time1);
        return result / 1000 / 60 / 60 / 24;
    }

    //获取次日时间
    public static String getTime() {
        Calendar calendar = Calendar.getInstance();
        //设置日期为今天+1，即等于第二天
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日");
        String format = simpleDateFormat.format(calendar.getTime());
        return format;
    }

    /**
     * 判断当前日期是星期几
     *
     * @param pTime 设置的需要判断的时间  //格式如2012-09-08
     * @return dayForWeek 判断结果
     * @Exception 发生异常
     */

//  String pTime = "2012-03-12";
    public static  String getWeek(String pTime) {


        String Week = "";


        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {

            c.setTime(format.parse(pTime));

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (c.get(Calendar.DAY_OF_WEEK)==1) {
            Week += "周天";
        }
        if (c.get(Calendar.DAY_OF_WEEK) == 2) {
            Week += "周一";
        }
        if (c.get(Calendar.DAY_OF_WEEK) == 3) {
            Week += "周二";
        }
        if (c.get(Calendar.DAY_OF_WEEK) == 4) {
            Week += "周三";
        }
        if (c.get(Calendar.DAY_OF_WEEK) == 5) {
            Week += "周四";
        }
        if (c.get(Calendar.DAY_OF_WEEK) == 6) {
            Week += "周五";
        }
        if (c.get(Calendar.DAY_OF_WEEK) == 7) {
            Week += "周六";
        }


        return Week;
    }


    /**
     * 获取月天
     * @param date
     * @return
     */
    public static String getMonthDay(String date){

        String s="";
        if (!TextUtils.isEmpty(date)){
            if (date.length()>9) {
                s = date.substring(5, 10);
            }
        }
        return s;
    }

    /**
     * 获取年月
     * @param date
     * @return
     */
    public static String getYearsMonth(String date){
        String s="";
        if (!TextUtils.isEmpty(date)){
            if (date.length()>7){
                s=date.substring(0,7);
            }

        }
        return s;
    }

    public static String getYearsMonthDay(String date){
        String s="";
        if (!TextUtils.isEmpty(date)){
            if (date.length()>9) {
                s = date.substring(0, 10);
            }
        }
        return s;
    }



    /**
     * 获取当前时间
     */
    public static String getCurrentTime(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//
        //获取当前时间
        Date date = new Date(System.currentTimeMillis());
        return simpleDateFormat.format(date);
    }
    /**
     * 获取当前时间
     */
    public static Date getCurrentDate(){
        //获取当前时间
        return new Date(System.currentTimeMillis());
    }

    /**
     * @Title: isInDate
     * @Description: 判断一个时间段（YYYY-MM-DD）是否在一个区间
     * @param @param date
     * @param @param strDateBegin
     * @param @param strDateEnd
     * @param @return    设定文件
     * @return boolean    返回类型
     * @throws
     */
    public static boolean isInDate(Date date, String strDateBegin,String strDateEnd) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String strDate = sdf.format(date);   //2017-04-11
        // 截取当前时间年月日 转成整型
        int  tempDate=Integer.parseInt(strDate.split("-")[0]+strDate.split("-")[1]+strDate.split("-")[2]);
        // 截取开始时间年月日 转成整型
        int  tempDateBegin=Integer.parseInt(strDateBegin.split("-")[0]+strDateBegin.split("-")[1]+strDateBegin.split("-")[2]);
        // 截取结束时间年月日   转成整型
        int  tempDateEnd=Integer.parseInt(strDateEnd.split("-")[0]+strDateEnd.split("-")[1]+strDateEnd.split("-")[2]);

        if ((tempDate >= tempDateBegin && tempDate <= tempDateEnd)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 获取当前月第一天
     * @return
     */
    public static String getCurrentMonthFirstDay(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.DAY_OF_MONTH, 1);
        return new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
    }

    /**
     * 获取当前月最后一天
     * @return
     */
    public static String getCurrentMonthLastDay(){
        Calendar ca = Calendar.getInstance();
        ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
        return new SimpleDateFormat("yyyy-MM-dd").format(ca.getTime());
    }

    /**
     * 获取上amount个月第一天
     * @param @param amount
     * 上一个月  amount=-1
     * 上二个月  amount=-2
     * 上三个月  amount=-3
     */
    public static String getPrecedingMonthFirstDay(int amount){
        Calendar   cal_1=Calendar.getInstance();//获取当前日期
        cal_1.add(Calendar.MONTH, amount);
        cal_1.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天
        return new SimpleDateFormat("yyyy-MM-dd").format(cal_1.getTime());
    }
    /**
     * 获取上amount个月最后一天
     * @param @param amount
     * 上一个月  amount=-1
     * 上二个月  amount=-2
     * 上三个月  amount=-3
     */
    public static String getPrecedingMonthLastDay(int amount){
        Calendar cale = Calendar.getInstance();
        cale.add(Calendar.MONTH, amount+1);
        cale.set(Calendar.DAY_OF_MONTH,0);//设置为1号,当前日期既为本月第一天
        return new SimpleDateFormat("yyyy-MM-dd").format(cale.getTime());
    }


    /**
     *求差，currentTime减去lastTime这个时间
     * @param currentTime
     * @param lastTime
     * @return
     */
    public static long subtract(String currentTime,String lastTime){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            Date d1 = df.parse(currentTime);
            Date d2 = df.parse(lastTime);
            long diff = d1.getTime() - d2.getTime();//这样得到的差值是微秒级别
            long days = diff / (1000 * 60 * 60 * 24);

            long hours = (diff-days*(1000 * 60 * 60 * 24))/(1000* 60 * 60);
            long minutes = (diff-days*(1000 * 60 * 60 * 24)-hours*(1000* 60 * 60))/(1000* 60);
            System.out.println(""+days+"天"+hours+"小时"+minutes+"分");
            return minutes;
        }catch (Exception e)
        {
            return 5;
        }


    }
}
