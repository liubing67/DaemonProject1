package example.fussen.daemon.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import example.fussen.daemon.App;

/**
 * Created by Administrator on 2015/12/3.
 */

public class SPUtils {
    /**
     * 保存在手机里面的文件名
     */
    public static final String FILE_NAME = "share_data";
    public static String LASTTIME = "lasttime";//最后请求的时间
    public static String ISFRIST = "isFrist";//判断是否是第一次
    public static String aaaaaaaaa = "aaaaa";//判断是否是第一次
    public static String bbbbbbbbbbbbb = "bbbbb";//判断是否是第一次

    /**
     * 保存数据的方法，我们需要拿到保存数据的具体类型，然后根据类型调用不同的保存方法
     *
     * @param context
     * @param key
     * @param object
     */
    public static void put(Context context, String key, Object object) {

        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_MULTI_PROCESS );
        SharedPreferences.Editor editor = sp.edit();
        if (object instanceof String) {
            editor.putString(key, (String) object);
        } else if (object instanceof Integer) {
            editor.putInt(key, (Integer) object);
        } else if (object instanceof Boolean) {
            editor.putBoolean(key, (Boolean) object);
        } else if (object instanceof Float) {
            editor.putFloat(key, (Float) object);
        } else if (object instanceof Long) {
            editor.putLong(key, (Long) object);
        } else {
            editor.putString(key, object.toString());
        }

        SharedPreferencesCompat.apply(editor);
    }

    /**
     * 判断key是否存在
     */
    public static boolean judeeKey(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_MULTI_PROCESS );
        if (sp.contains(key)) {
            return true;
        }
        return false;
    }

    /**
     * 得到保存数据的方法，我们根据默认值得到保存的数据的具体类型，然后调用相对于的方法获取值
     *
     * @param context
     * @param key
     * @param defaultObject
     * @return
     */
    public static Object get(Context context, String key, Object defaultObject) {
        if (context == null) {
            context = App.getInstance();
        }

        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_MULTI_PROCESS);

        if (defaultObject instanceof String) {
            return sp.getString(key, (String) defaultObject);
        } else if (defaultObject instanceof Integer) {
            return sp.getInt(key, (Integer) defaultObject);
        } else if (defaultObject instanceof Boolean) {
            return sp.getBoolean(key, (Boolean) defaultObject);
        } else if (defaultObject instanceof Float) {
            return sp.getFloat(key, (Float) defaultObject);
        } else if (defaultObject instanceof Long) {
            return sp.getLong(key, (Long) defaultObject);
        }

        return null;
    }

    /**
     * 移除某个key值已经对应的值
     *
     * @param context
     * @param key
     */
    public static void remove(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(key);
        SharedPreferencesCompat.apply(editor);
    }

    /**
     * 清除所有数据
     *
     * @param context
     */
    public static void clear(Context context) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        SharedPreferencesCompat.apply(editor);
    }

    /**
     * 查询某个key是否已经存在
     *
     * @param context
     * @param key
     * @return
     */
    public static boolean contains(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_MULTI_PROCESS);
        return sp.contains(key);
    }

    /**
     * 返回所有的键值对
     *
     * @param context
     * @return
     */
    public static Map<String, ?> getAll(Context context) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_MULTI_PROCESS);
        return sp.getAll();
    }

    /**
     * 创建一个解决SharedPreferencesCompat.apply方法的一个兼容类
     *
     * @author zhy
     */
    private static class SharedPreferencesCompat {
        private static final Method sApplyMethod = findApplyMethod();

        /**
         * 反射查找apply的方法
         *
         * @return
         */
        @SuppressWarnings({"unchecked", "rawtypes"})
        private static Method findApplyMethod() {
            try {
                Class clz = SharedPreferences.Editor.class;
                return clz.getMethod("apply");
            } catch (NoSuchMethodException e) {
            }

            return null;
        }

        /**
         * 如果找到则使用apply执行，否则使用commit
         *
         * @param editor
         */
        public static void apply(SharedPreferences.Editor editor) {
            try {
                if (sApplyMethod != null) {
                    sApplyMethod.invoke(editor);
                    return;
                }
            } catch (IllegalArgumentException e) {
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e) {
            }
            editor.commit();
        }
    }

    /**
     * 保存List
     *
     * @param tag
     * @param datalist
     */
//    public static void setDataList(Context context, String tag, List<SortBean> datalist) {
//        SharedPreferences sp = context.getSharedPreferences(SPUtils.get(context, SPUtils.USERID, FILE_NAME).toString(),
//                Context.MODE_MULTI_PROCESS);
//        SharedPreferences.Editor editor = sp.edit();
//        if (null == datalist) {
//            return;
//        }
//
//
//        Gson gson = new Gson();
//        //转换成json数据，再保存
//        String strJson = gson.toJson(datalist);
//        editor.putString(tag, strJson);
//        editor.commit();
//
//    }

    /**
     * 获取lIst
     *
     * @param tag
     * @return
     */
//    public static List<SortBean> getDataList(Context context, String tag) {
//        SharedPreferences sp = context.getSharedPreferences(SPUtils.get(context, SPUtils.USERID, FILE_NAME).toString(),
//                Context.MODE_MULTI_PROCESS);
//        List<SortBean> datalist = new ArrayList<SortBean>();
//        String strJson = sp.getString(tag, null);
//        if (null == strJson) {
//            return datalist;
//        }
//        Gson gson = new Gson();
//        datalist = gson.fromJson(strJson, new TypeToken<List<SortBean>>() {
//        }.getType());
//        return datalist;
//
//    }

    private static final String DEFAULT_SP_NAME = "default_sp";

    /**
     * 保存一个对象
     * 我们以对象的类名字作为key，以对象的json字符串作为value保存到SharePreference中。
     *
     * @param context
     * @param object
     */
//    public static void putObject(Context context, Object object) {
//        String key = getKey(object.getClass());
//        Gson gson = new Gson();
//        String json = gson.toJson(object);
//        putString(context, key, json);
//    }

    /**
     * 获取对象
     * 我们先获取类的名字，再将它作为key，然后从SharePreference中获取对应的字符串，然后通过Gson将json字符串转化为对象
     *
     * @param context
     * @param clazz
     * @param <T>
     * @return
     */
//    public static <T> T getObject(Context context, Class<T> clazz) {
//        String key = getKey(clazz);
//        String json = getString(context, key, null);
//        if (TextUtils.isEmpty(json)) {
//            return null;
//        }
//        try {
//            Gson gson = new Gson();
//            return gson.fromJson(json, clazz);
//        } catch (Exception e) {
//            return null;
//        }
//    }

    public static String getKey(Class<?> clazz) {
        return clazz.getName();
    }

    public static void putString(Context context, String key, String value) {
        SharedPreferences sp = context.getSharedPreferences(DEFAULT_SP_NAME, Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public static String getString(Context context, String key, String defValue) {
        SharedPreferences sp = context.getSharedPreferences(DEFAULT_SP_NAME, Context.MODE_MULTI_PROCESS);
        return sp.getString(key, defValue);
    }

    /**
     * 移除对象
     *
     * @param context
     * @param clazz
     */
    public static void removeObject(Context context, Class<?> clazz) {
        removeobj(context, getKey(clazz));
    }

    public static void removeobj(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(DEFAULT_SP_NAME, Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor edit = sp.edit();
        edit.remove(key);
        edit.commit();
    }
}

